package it.fitstic_edu.java.entity;

public class SimPinManager {
    protected void pinCompare(Sumaho instancedPhone) {

        while (instancedPhone.getNumTries() > 0) {

            int pinTry = instancedPhone.getSim().getPin().getInt("Digit pin to unlock the phone\nMust be 6 digits\n:", 99000, 1000000);

            if (pinTry == instancedPhone.getSim().getPin().getPinNumeric()) {

                instancedPhone.setUnlocked(true);
                System.out.println("Phone is now unlocked");
                return;

            } else {

                instancedPhone.setNumTries(instancedPhone.getNumTries() - 1);
                System.out.println("Wrong number," + instancedPhone.getNumTries() + " tries left");

            }
        }
        System.out.println("No more tries left, phone is now locked.");

    }

    protected void putSimIn(Sumaho instancedPhone) {
        if (instancedPhone.isSimIn()) {

            System.out.println("Your sim is already in the phone.");
        } else {

            insertSim(instancedPhone);
        }
    }


    /**
     * Manages the act of putting the sim in the phone.
     * check inline comments for reference
     * 1) Ask the user if he knows the number already or not
     * 2) If he does, does he also know the pin?
     * 3) Yes? Ask for pin and number
     * 4) Check if the number are actually numbers;
     * 5) in the right format (6 digit pin and 10 digits number)
     * 6) User doesn't know the pin so the code will call the correct sim constructor
     * 7) User doesn't know anything so the code will generate a random number and pin
     */

    private void insertSim(Sumaho instancedPhone) {

        String telephone;
        String pin;

        //1

        while (true) {
            System.out.println("Do you know your number already?\ny for yes n for no\n:");
            telephone = PIN.getInput("").toLowerCase();
            String number;
            long num;

            if (telephone.equals("y")) {
                //2
                pin = PIN.getInput("Do you know your pin as well?\ny for yes n for no\n:").toLowerCase();

                if (pin.equals("y")) {
                    int code;

                    while (true) {

                        //3
                        number = PIN.getInput("What's your number?\n:");
                        pin = PIN.getInput("What's your pin?\n:");

                        //4
                        try {


                            num = Long.parseLong(number);
                            code = Integer.parseInt(pin);

                            //5
                            if (num > 99999999L) {

                                if (code > 99999 && code < 1000000) {

                                    instancedPhone.setSim(new SIM(num, code));
                                    System.out.println("Your number " + num + " has been paired with the phone with this pin " + instancedPhone.getSim().getPin().getPinNumeric());
                                    instancedPhone.setSimIn(true);
                                    return;
                                }
                            }

                        } catch (Exception e) {
                            System.out.println("Numero non valido, deve avere 10 cifre, ed il pin 6 cifre");
                        }
                    }

                    //6
                } else if (pin.equalsIgnoreCase("n")) {

                    number = PIN.getInput("What's your number?\n:");

                    try {
                        //numero += "L";
                        num = Long.parseLong(number);

                        if (num > 99999999L) {

                            instancedPhone.setSim(new SIM(num));
                            System.out.println("Your number " + num + " has been paired with the phone with this pin " + instancedPhone.getSim().getPin().getPinNumeric());
                            instancedPhone.setSimIn(true);
                            return;
                        }

                    } catch (Exception e) {

                        System.out.println("Numero non valido, deve avere 10 cifre");
                    }
                }

            }
            //7
            else if (telephone.equals("n")) {

                instancedPhone.setSim(new SIM());
                System.out.println("Your number " + instancedPhone.getSim().getNumber() + " has been paired with the phone with this pin " + instancedPhone.getSim().getPin().getPinNumeric());
                instancedPhone.setSimIn(true);
                return;

            }
        }

    }

    protected void changeSimCode(Sumaho instancedPhone) {
        if (instancedPhone.isUnlocked() && !instancedPhone.isPhoneOff() && instancedPhone.isSimIn()) {

            instancedPhone.getSim().changePin();
        } else {

            if (instancedPhone.isPhoneOff()) {

                System.out.print("Phone off\n");
            } else {
                System.out.print("Phone locked\n");
            }
        }
    }

    protected void showNumber(Sumaho instancedPhone) {
        if (instancedPhone.isSimIn() && !instancedPhone.isPhoneOff()) {

            System.out.println("Your Number is " + instancedPhone.getSim().getNumber());

        } else {

            System.out.println("There's no number paired with this phone. or your phone is off");
        }
    }

    protected void takeSimOut(Sumaho instancedPhone) {
        if (instancedPhone.isSimIn()) {

            instancedPhone.setSimIn(false);
            instancedPhone.setSim(new SIM(instancedPhone.getDefaultSim(), instancedPhone.getDefaultPin()));

            instancedPhone.setUnlocked(false);
            System.out.println("Your sim is no out of the phone and number has been unpaired.");


        } else {

            System.out.println("There's no sim in this phone.");
        }
    }

    protected void showPin(Sumaho instancedPhone) {
        if (instancedPhone.isSimIn() && !instancedPhone.isPhoneOff() && instancedPhone.isUnlocked()) {

            System.out.println("Your PIN is " + instancedPhone.getSim().getPin().getPinNumeric());

        } else {

            System.out.println("Your phone is either off, locked, or there's no sim in the phone");
        }
    }

    protected void unlockPhone(Sumaho instancedPhone) {
        if (instancedPhone.isSimIn() && !instancedPhone.isPhoneOff() && instancedPhone.isUnlocked()) {

            pinCompare(instancedPhone);

        } else {

            if (instancedPhone.isUnlocked()) {

                System.out.println("Your phone is already unlocked");
            } else if (!instancedPhone.isSimIn()) {

                System.out.println("There's no sim in your phone");
            } else {

                System.out.println("You phone is off");

            }
        }
    }

}
