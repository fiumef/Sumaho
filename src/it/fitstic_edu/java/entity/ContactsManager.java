package it.fitstic_edu.java.entity;

import java.util.Map;

public class ContactsManager {
    protected void goToContacts(Sumaho inStanceofPhone) {
        int whatToDo;
        if (inStanceofPhone.isSimIn() && inStanceofPhone.isUnlocked() && !inStanceofPhone.isPhoneOff()) {
            while (true) {

                whatToDo = inStanceofPhone.getSim().getPin().getInt("What do you want to do?\n1: See all the contacts\n2: Add a contact to your list\n3: remove contact from your list\n4: find a specific contact in your list\n5: modify contact\n6: get out\n:", 1, 6);
                System.out.println("________");
                switch (whatToDo) {
                    case 1:
                        checkContacts(inStanceofPhone);
                        break;
                    case 2:

                        addContacts(inStanceofPhone);
                        break;
                    case 3:
                        removeContacts(inStanceofPhone);
                        break;
                    case 4:
                        findContacts(inStanceofPhone);
                        break;
                    case 5:
                        modifyContact(inStanceofPhone);
                        break;
                    case 6:
                        return;

                }
                System.out.println("________");
            }
        } else {
            if (!inStanceofPhone.isSimIn()) {
                System.out.println("There's no sim in the phone.");
            } else if (!inStanceofPhone.isUnlocked()) {
                System.out.println("Unlock phone first");
            } else {
                System.out.println("Phone needs to be on");
            }
        }
    }

    private void checkContacts(Sumaho inStanceofPhone) {
        for (Map.Entry<String, Long> set : inStanceofPhone.getContacts().entrySet()) {
            System.out.println(set.getKey().substring(0, 1).toUpperCase() + set.getKey().substring(1) + " = " + set.getValue());
        }
    }

    private void addContacts(Sumaho inStanceofPhone) {

        long number;

        String name = PIN.getInput("What's the name ?\n:").toLowerCase();
        for (Map.Entry<String, Long> set : inStanceofPhone.getContacts().entrySet()) {
            if (set.getKey().equals(name)) {
                System.out.println("You can't have multiple contacts with the same name.");
                return;
            }
        }
        while (true) {
            String getNumber = PIN.getInput("What's the number?\n:");

            try {


                number = Long.parseLong(getNumber);


                //5
                if (number > 99999999L) {
                    for (Map.Entry<String, Long> set : inStanceofPhone.getContacts().entrySet()) {
                        if (set.getValue() == number) {
                            System.out.println("You can't have multiple contacts with the same name.");
                            return;
                        }
                    }
                    inStanceofPhone.getContacts().put(name, number);
                    return;

                }

            } catch (Exception e) {
                System.out.println("Invalid Number");
            }

        }
    }

    private void removeContacts(Sumaho inStanceofPhone) {
        String name = PIN.getInput("What's the name of the contact you want to remove?\n:").toLowerCase();
        try {
            inStanceofPhone.getContacts().remove(name);
            System.out.println("You've successfully removed: " + name.substring(0, 1).toUpperCase() + name.substring(1));

        } catch (Exception e) {
            System.out.println("Name not found, print all the contacts and put in the correct name");

        }


    }

    private void findContacts(Sumaho inStanceofPhone) {
        String name = PIN.getInput("What's the name of the contact you want to find?\n:").toLowerCase();
        try {
            inStanceofPhone.getContacts().get(name);
            System.out.println(name.substring(0, 1).toUpperCase() + name.substring(1) + " = " + inStanceofPhone.getContacts().get(name));
        } catch (Exception e) {
            System.out.println("Name not found, print all the contacts and put in the correct name");
        }
    }

    private void modifyContact(Sumaho inStanceofPhone) {
        String name = PIN.getInput("What's the name of the contact you want to modify?\n:").toLowerCase();
        int whatToModify = inStanceofPhone.getSim().getPin().getInt("What do you want to change?\n1: Name\n2: Number\n:", 1, 2);

        switch (whatToModify) {
            case 1:
                String newName = PIN.getInput("What's the new name of the contact?\n:").toLowerCase();
                try {
                    long number = inStanceofPhone.getContacts().get(name);
                    inStanceofPhone.getContacts().remove(name);
                    inStanceofPhone.getContacts().put(newName, number);

                } catch (Exception e) {
                    System.out.println("Name not found");

                }
                break;

            case 2:
                while (true) {
                    long number;
                    String getNumber = PIN.getInput("What's the new number?\n:");

                    try {


                        number = Long.parseLong(getNumber);


                        //5
                        if (number > 99999999L) {
                            inStanceofPhone.getContacts().remove(name);
                            inStanceofPhone.getContacts().put(name, number);
                            return;

                        }

                    } catch (Exception e) {
                        System.out.println("Invalid Number");
                    }

                }


        }


    }

}
