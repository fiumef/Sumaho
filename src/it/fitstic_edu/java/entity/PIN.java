package it.fitstic_edu.java.entity;

import java.util.Random;
import java.util.Scanner;

public class PIN {
    /**
     * Just a pin-code
     */
    private int pin;

    /**
     * if the pin constructor gets called without any parameter it will generate a random 6 digits code
     */
    public PIN() {

        genPin();
    }

    /**
     * @param pin if the pin is known already to the user then the constructor will use the number that the user will input
     */
    public PIN(int pin) {

        this.pin = pin;
    }

    /**
     * Generate a random 6 digits code
     */
    protected void genPin() {

        Random r = new Random();
        int newPin = r.nextInt(100000, 999999);
        this.pin = newPin;
    }

    /**
     * @param newPin Is the number that the user sets as new pin
     *               newpin will replace the former pin as the new pin code
     */
    protected void changePin(int newPin) {

        this.pin = newPin;
    }

    /**
     * @param x is the string that you want to prompt the user with
     *          This function will get an user input and return it as a string
     */
    protected static String getInput(String x) {

        Scanner scanner = new Scanner(System.in);
        System.out.print(x);
        String in = scanner.nextLine();
        return in;
    }

    /**
     * @param x          is the string that you want to prompt the user with
     * @param StartRange is the starting range of the number you want to get
     * @param endRange   is the ending range of the number you want
     *                   just makes sure the number you input is actually a number
     */
    protected int getInt(String x, int StartRange, int endRange) {

        int provan = 0;
        while (true) {
            String input = getInput(x);
            try {
                provan = Integer.parseInt(input);
                if (provan >= StartRange && provan <= endRange) {
                    return provan;
                }
            } catch (Exception e) {
                System.out.println("Select a number in the range provided");
            }
        }
    }

    /**
     * return the pin and allow to access it securely from outside the class
     */
    protected int getPinNumeric() {

        return this.pin;
    }
}
