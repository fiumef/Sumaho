package it.fitstic_edu.java.entity;

import java.util.HashMap;

/**
 * here is where the juice at
 */
public class Sumaho {
    /**
     * Sim istance
     */
    private SIM sim;
    private final ContactsManager contactsManager = new ContactsManager();
    private final SaveLoadPhoneState saveloadphonestate = new SaveLoadPhoneState();
    private final SimPinManager simpinmanager = new SimPinManager();

    private HashMap<String, Long> Contacts = new HashMap<>();
    private final HashMap<Long, Integer> numPinPair = new HashMap<Long, Integer>();
    private final long defaultSim = 0000000000;
    private final int defaultPin = 000000;
    /**
     * Amount of codes that you are allowed to enter before the phone gets locked. Don't lose your pin.
     */
    private final int DEFAULT_NUMTRIES = 5;
    private int numTries = 5;


    /**
     * tracks wether the sim is in the phone or not
     */
    private boolean simIn = false;


    /**
     * tracks wether the phone is on or not
     */
    private boolean phoneOff = true;


    /**
     * tackes wether the phone is locked or not
     */
    private boolean Unlocked = false;


    /**
     * just a String for the meny comfy to modify
     */
    private final String menu = "What do you want to do?\n1: Turn on Phone\n" +
            "2: Insert SIM\n3: Change PIN\n4: Turn Off Phone\n5: Take SIM Out\n" +
            "6: Show Number\n7: Show PIN\n8: Unlock phone\n9: Check all\n" +
            "10: Go to Contacts manager\n";


    private final int TURN_PHONE_ON = 1;
    private final int INSERT_SIM = 2;
    private final int CHANGE_PIN = 3;
    private final int TURN_PHONE_OFF = 4;
    private final int TAKE_SIM_OUT = 5;
    private final int SHOW_NUMBER = 6;
    private final int SHOW_PIN = 7;
    private final int UNLOCK_PHONE = 8;
    private final int CHECK_ALL = 9;
    private final int GO_TO_CONTACTS = 10;


    private final String loadState = "src/it/fitstic_edu/java/entity/Data/loadState.json";


    /**
     * you need the sim istance to be running for the code not to be broken,
     */
    public Sumaho() {
        sim = new SIM();
    }


    private Sumaho(boolean simIn, boolean Unlocked, boolean phoneOff, int numTries, HashMap Contacts, SIM sim) {
        this.Unlocked = Unlocked;
        this.phoneOff = phoneOff;
        this.simIn = simIn;
        this.numTries = numTries;
        this.Contacts = Contacts;
        this.sim = sim;
    }

    /**
     * compares the pin that the user inputs with the actual one and if it's correct unlocks the phone
     */


    /**
     * Prints a list of info that are allowed to be known in the current state of the phone
     */
    private void getPhoneInfo() {

        if (simIn && !phoneOff && Unlocked) {
            System.out.println("Your phone is on, there's one sim paired.\n" +
                    "Your number is " + sim.getNumber() + " and your pin is " + sim.getPin().getPinNumeric());
        } else if (simIn && phoneOff && !Unlocked) {
            System.out.println("Your phone is on, there's one sim paired.\n" +
                    "Your number is " + sim.getNumber() +
                    " but I can't know your pin as long as the phone will be locked");
        } else if (simIn && phoneOff) {
            System.out.println("Your phone is off, there's one sim paired.\n" +
                    "Your number is " + sim.getNumber() +
                    " but I can't know your pin as long as the phone will be off");
        } else if (!simIn && phoneOff) {
            System.out.println("There's no sim in the phone and the phone is off");
        } else {
            System.out.println("No info available");
        }


    }

    private void turnPhoneOn() {
        if (phoneOff) {

            phoneOff = false;
            System.out.println("Phone is now on");
            if (simIn) {

                simpinmanager.pinCompare(this);

            }

        } else {

            System.out.println("Phone already on");
        }
    }

    private void turnPhoneOff() {
        if (!phoneOff) {

            phoneOff = true;
            Unlocked = false;
            System.out.println("Phone is now off");
            numTries = DEFAULT_NUMTRIES;
        } else {

            System.out.println("Phone is off already");
        }
    }

    /**
     * This function is the bread and butter of the program, uses the menu string to get a number as input and then works
     * toward that number.
     * select = 1) Check if phone is off and turns it on and asks for the pin
     * select =2) Check if sim in the phone, if it's not calls the insertSim function
     * select = 3) allowes to change the pin if the phone is on, unlocked and the sim is in
     * select = 4) Turn phone down and reset relative attributes (phoneoff is now true and unlocked is false)
     * select = 5) if there's a sim in the phone, the sim gets a null value and all the attributes are reset (aside from phoneoff)
     * select = 6) Gets telephone number if the sim is in and the phone is on
     * select = 7)Gets pin info if allowed
     * select = 8) Allowes to unlock the phone if you changed the sim and don't want to turn phone off and on
     * select = 9) Print all the phone info that the phone state allowes for (see getPhoneInfo())
     */
    public void canDo() {

        int select;
        saveloadphonestate.askForLoadState(this);

        while (true) {
            saveloadphonestate.savePhoneState(this);
            System.out.println("___________");
            System.out.print(menu);
            select = sim.getPin().getInt(":", 1, 11);
            switch (select) {
                case TURN_PHONE_ON:
                    turnPhoneOn();
                    break;
                case INSERT_SIM:
                    simpinmanager.putSimIn(this);
                    break;
                case CHANGE_PIN:
                    simpinmanager.changeSimCode(this);
                    break;
                case TURN_PHONE_OFF:
                    turnPhoneOff();
                    break;
                case TAKE_SIM_OUT:
                    simpinmanager.takeSimOut(this);
                    break;
                case SHOW_NUMBER:
                    simpinmanager.showNumber(this);
                    break;
                case SHOW_PIN:
                    simpinmanager.showPin(this);
                    break;
                case UNLOCK_PHONE:
                    simpinmanager.unlockPhone(this);
                    break;
                case CHECK_ALL:
                    getPhoneInfo();
                    break;
                case GO_TO_CONTACTS:
                    contactsManager.goToContacts(this);
                    break;
            }
        }
    }

    //Getters and setters
    public SIM getSim() {
        return sim;
    }

    public void setSim(SIM sim) {
        this.sim = sim;
    }

    public boolean isSimIn() {
        return simIn;
    }

    public void setSimIn(boolean simIn) {
        this.simIn = simIn;
    }

    public int getNumTries() {
        return numTries;
    }

    public void setNumTries(int numTries) {
        this.numTries = numTries;
    }

    public boolean isPhoneOff() {
        return phoneOff;
    }

    public void setPhoneOff(boolean phoneOff) {
        this.phoneOff = phoneOff;
    }

    public boolean isUnlocked() {
        return Unlocked;
    }

    public void setUnlocked(boolean unlocked) {
        Unlocked = unlocked;
    }

    public HashMap<String, Long> getContacts() {
        return Contacts;
    }

    public void setContacts(HashMap<String, Long> contacts) {
        Contacts = contacts;
    }

    public String getLoadState() {
        return loadState;
    }

    public long getDefaultSim() {
        return defaultSim;
    }

    public int getDefaultPin() {
        return defaultPin;
    }
}