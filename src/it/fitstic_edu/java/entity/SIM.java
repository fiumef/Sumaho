package it.fitstic_edu.java.entity;

import java.util.Random;

public class SIM {
    /**
     * phone number
     */
    private final long number;

    /**
     * istance of the pin class, very useful
     */
    private final PIN pin;

    /**
     * first constructor, the user knows both the pin and the number
     *
     * @param number, sets the number as the one that the user has
     * @param pin     same as number
     */
    public SIM(long number, int pin) {

        this.number = number;
        this.pin = new PIN(pin);
    }

    /**
     * second constructor, the user only knows the number
     *
     * @param number is known from the user
     *               the constructor will use the user's number and generate a brand new 6 digit pin for the user
     */
    public SIM(long number) {

        this.number = number;
        this.pin = new PIN();
    }

    /**
     * the user doesn't know neither, so the constructor will generate a new 6 digits pin and a new 10 digits phone number
     */
    public SIM() {

        this.number = genNumber();
        this.pin = new PIN();

    }

    /**
     * makes a brand new number
     */
    private long genNumber() {

        Random r = new Random();
        String t = "33812";
        String te;
        String tele;
        int tel = r.nextInt(10000, 100000);
        te = Integer.toString(tel);
        tele = t + te;
        long telef = Long.parseLong(tele);

        return telef;
    }

    /**
     * uses the function getInt from the pin class to get a pure 6 digits number and uses that code to replace the pin
     */
    protected void changePin() {
        int newPin = this.pin.getInt("What's the new pin?\nMust be 6 digits.\n:", 100000, 1000000);
        pin.changePin(newPin);
        System.out.println("Your new pin is now: " + newPin);

    }

    /**
     * allowes to access the class pin from outside the class sim
     */
    public PIN getPin() {
        return pin;
    }

    /**
     * allowes to get the number outside the class
     */
    protected long getNumber() {
        return number;
    }
}
