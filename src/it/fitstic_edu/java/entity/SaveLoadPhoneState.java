package it.fitstic_edu.java.entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class SaveLoadPhoneState {
    private Object fileMakerTaker(String x) {
        try {
            File file = new File(x);

            return file;


        } catch (Exception e) {
        }
        return null;
    }

    protected void askForLoadState(Sumaho instancedPhone) {

        File f = (File) fileMakerTaker(instancedPhone.getLoadState());
        if (f.exists()) {
            while (true) {
                String answer = PIN.getInput("I noticed there are previous data about the phone.\nDo you want to load them?\ny for yes\nn for no.\n:");
                if (answer.equals("y")) {
                    loadPhoneState(instancedPhone);
                    return;
                } else if (answer.equals("n")) {
                    answer = PIN.getInput("If you don't restore previous settings you will lose all the phone data. Do you wish to continue?\ny for yes\nAny key for no\n:");
                    if (answer.equals("y")) {
                        return;
                    }

                } else {
                    System.out.println("Invalid answer");
                }
            }
        }

    }

    private void loadPhoneState(Sumaho instancedPhone) {
        try {

            Gson gson = new Gson();
            FileReader fr = new FileReader(instancedPhone.getLoadState());
            try {
                Sumaho sumahoInstancedFromJson = gson.fromJson(fr, Sumaho.class);


                instancedPhone.setSim(sumahoInstancedFromJson.getSim());
                instancedPhone.setContacts(sumahoInstancedFromJson.getContacts());
                instancedPhone.setSimIn(sumahoInstancedFromJson.isSimIn());
                instancedPhone.setNumTries(sumahoInstancedFromJson.getNumTries());
                instancedPhone.setPhoneOff(sumahoInstancedFromJson.isPhoneOff());
                instancedPhone.setUnlocked(sumahoInstancedFromJson.isUnlocked());

                fr.close();
            } catch (Exception e) {
                System.out.println("Saved file is corrupted. Restoring from factory");
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    protected void savePhoneState(Sumaho instancedPhone) {
        try {
            File FiletoWriteTo = (File) fileMakerTaker(instancedPhone.getLoadState());
            Gson gson = new GsonBuilder().setPrettyPrinting().create();


            FileWriter fw = new FileWriter(FiletoWriteTo);
            gson.toJson(instancedPhone, fw);
            fw.close();


        } catch (Exception e) {
        }

    }
}
